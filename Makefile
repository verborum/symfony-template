docker-up:
	docker compose up -d

docker-down:
	docker compose down

docker-php-exec:
	docker compose exec php bash

install:
	docker compose exec php composer install
